# hyprland-cursor-lock

A silly workaround to lock the mouse cursor inside the currently focused window. 

Based on [hyprland-lol-silly-workaround](https://github.com/BKSalman/hyprland-lol-silly-workaround). Thanks to him for allowing me to adapt it for my use.

You can add the following binding to toggle the locking to your Hyprland configuration file.
```
bind = SUPER, F1, exec, killall hyprland-cursor-lock || hyprland-cursor-lock
```

## TODO
- [ ] Use a configuration file to have custom adjustement for the cursor size per client class name (currently it use an arbritary value that works for League Of Lengends)
- [ ] Use a argument (or in configuration file) to adjust the sleep (currently 1 ms)