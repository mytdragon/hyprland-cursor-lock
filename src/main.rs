use std::thread;
use std::time::Duration;
use hyprland::{
    data::{Client, CursorPosition},
    dispatch::{Dispatch, DispatchType},
    shared::{HyprData, HyprDataActiveOptional},
};

fn main() {
    // Used to feel like the cursor doesn't leave window
    // TODO: parse from a config file sleep, and for different adjustment for each window class name
    let sleep_time = 1;
    let adjust_x = 12;
    let adjust_y = 12;

    let active_client_result = Client::get_active();

    match active_client_result {
        Ok(Some(active_client)) => {
            loop {
                // Convert `i16` values to `i64` before using them.
                let at_x = active_client.at.0 as i64;
                let at_y = active_client.at.1 as i64;
                let size_x = active_client.size.0 as i64;
                let size_y = active_client.size.1 as i64;

                // Get the maximum X coordinate based on the size of the active client's window.
                let max_x = at_x + size_x;
                let max_y = at_y + size_y;
                let cursor_pos = CursorPosition::get().unwrap();

                // Check if the cursor x-coordinate is outside the left boundary of the window.
                if cursor_pos.x < at_x {
                    // Move the cursor to the left boundary of the window.
                    Dispatch::call(DispatchType::MoveCursor(at_x, cursor_pos.y)).unwrap();
                    println!("Cursor moved to x: {} y: {}", at_x, cursor_pos.y);
                }
                // Check if the cursor x-coordinate is outside the right boundary of the window.
                else if cursor_pos.x > max_x - adjust_x {
                    // Move the cursor to the right boundary of the window.
                    Dispatch::call(DispatchType::MoveCursor(max_x - adjust_x, cursor_pos.y)).unwrap();
                    println!("Cursor moved to x: {} y: {}", max_x - adjust_x, cursor_pos.y);
                }

                // Check if the cursor y-coordinate is outside the top boundary of the window.
                if cursor_pos.y < at_y {
                    // Move the cursor to the left boundary of the window.
                    Dispatch::call(DispatchType::MoveCursor(cursor_pos.x, at_y)).unwrap();
                    println!("Cursor moved to x: {} y: {}", cursor_pos.x, at_y);
                }
                // Check if the cursor x-coordinate is outside the bottom boundary of the window.
                else if cursor_pos.y > max_y - adjust_y {
                    // Move the cursor to the right boundary of the window.
                    Dispatch::call(DispatchType::MoveCursor(cursor_pos.x, max_y - adjust_y)).unwrap();
                    println!("Cursor moved to x: {} y: {}", cursor_pos.x, max_y - adjust_y);
                }

                thread::sleep(Duration::from_millis(sleep_time));
            }
        }
        Ok(None) => {
            println!("No active window.");
        }
        Err(error) => {
            eprintln!("Error: {:?}", error);
        }
    }
}
